from marshmallow import Schema, validate, fields

class UserSchema(Schema):
    fullname = fields.String(required=True, validate=[
        validate.Length(max=250)])
    login = fields.String(required=True, validate=[
        validate.Length(max=50)])
    password = fields.String(required=True, validate=[
        validate.Length(max=100)], load_only=True)
    message = fields.String(dump_only=True)


class AuthSchema(Schema):
    access_token = fields.String(dump_only=True)
    message = fields.String(dump_only=True)
