from . import Base
import sqlalchemy as db
from flask_jwt_extended import create_access_token
from datetime import timedelta
from passlib.hash import bcrypt


class User(Base):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(250))
    login = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(100))

    def __init__(self, **kwargs):
        self.fullname = kwargs.get('fullname')
        self.login = kwargs.get('login')
        self.password = bcrypt.hash(kwargs.get('password'))
    
    def get_token(self, expire_time=24):
        expire_delta = timedelta(expire_time)
        token = create_access_token(
            identity=self.login, expires_delta=expire_delta)
        return token
    
    @classmethod
    def authenticate(cls, login, password):
        user = cls.query.filter(cls.login == login).one()
        if not bcrypt.verify(password, user.password):
            raise Exception('Нет пользователя с таким паролем.'
                            'Попробуйте ещё раз.')
        return user

    def __repr__(self):
        return f'{self.fullname}'