from flask import Blueprint, jsonify
from flaskr import logger, session, docs
from flaskr.schemas import UserSchema, AuthSchema
from flask_apispec import use_kwargs, marshal_with
from flaskr.models import User
from sqlalchemy.exc import IntegrityError

users = Blueprint('users', __name__)

@users.route('/register', methods=['POST'])
@use_kwargs(UserSchema)
def register(**kwargs):
    user = User(**kwargs)
    try:
        session.add(user)
        session.commit()
        message = 'Successful registration.'
        return {'message': message}, 201
    except IntegrityError as e:
        session.rollback()
        logger.warning(
            f'Ошибка регистрации: {e}')
        return {'message': str(e)}, 400


@users.route('/login', methods=['POST'])
@use_kwargs(UserSchema(only=('login', 'password')))
@marshal_with(AuthSchema)
def login(**kwargs):
    try:    
        user = User.authenticate(**kwargs)
        token = user.get_token()
        return {'access_token': token}
    except Exception as e:
        logger.warning(
            f'Ошибка входа для логина {kwargs["login"]}: {e}')
        return {'message': str(e)}, 400


@users.errorhandler(422)
def handle_error(err):
    headers = err.data.get('headers', None)
    messages = err.data.get('messages', ['Неверный запрос. Попробуйте ещё раз.'])
    logger.warning(f'Неверный ввод данных: {messages}')
    if headers:
        return jsonify({'message': messages}), 400, headers
    else:
        return jsonify({'message': messages}), 400

docs.register(register, blueprint='users')
docs.register(login, blueprint='users')
