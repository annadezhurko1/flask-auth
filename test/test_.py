from flaskr import *

def test_reg():
    data = {"fullname": "testfullname",
            "login": "testlogin",
            "password": "testpassword"
    }
    response = client.post('/register',
                           json = data
        )
    assert response.status_code == 201
    assert b'Successful registration.' in response.data