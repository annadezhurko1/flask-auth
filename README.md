## flask-auth
API-сервис регистрации и авторизации пользователя.

Реализован с использованием микрофреймворка Flask для хранения информации используется  СУБД Postgres.

Доступна регистрация пользователей, аунтетификация по логину и паролю.


### Технологический стек
[![Python](https://img.shields.io/badge/-Python-464646?style=flat&logo=Python&logoColor=56C0C0&color=0095b6)](https://www.python.org/)
[![Flask](https://img.shields.io/badge/-Flask-464646?style=flat&logo=Flask&logoColor=56C0C0&color=0095b6)](https://flask.palletsprojects.com/en/3.0.x/tutorial/)
[![SQLAlchemy](https://img.shields.io/badge/-SQLAlchemy-464646?style=flat&logo=SQLAlchemy&logoColor=56C0C0&color=0095b6)](https://www.sqlalchemy.org/)
[![marshmallow](https://img.shields.io/badge/-marshmallow-464646?style=flat&logo=marshmallow&logoColor=56C0C0&color=0095b6)](https://marshmallow.readthedocs.io/en/stable/)
[![apispec](https://img.shields.io/badge/-apispec-464646?style=flat&logo=apispec&logoColor=56C0C0&color=0095b6)](https://apispec.readthedocs.io/en/latest/)


### Документация по стандарту Open API:
http://127.0.0.1:5000/swagger-ui/


### Установка и запуск:
```bash
git clone git@gitlab.com:annadezhurko1/flask-auth.git
cd flask_auth
python -m venv env
source venv/bin/activate
pip install -r requirements.txt

python manage.py
```
